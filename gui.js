var spawn   = require('child_process').spawn;
var fs      = require('fs');
var ps      = require('ps-node');


var GUI     =   {};

var meteorProcess = null;

var outputHandler = function () {};

var runningWatcher = function () {};

var readyHandler   = function () {};

var meteorDirectoryValid = false;

var nginxRunning         = false;

process.env['ROOT_URL'] = 'http://localhost';

var workingDirectory = process.env.OLDPWD;

function readyToStart () {
    return meteorDirectoryValid && nginxRunning;
}

GUI.meteorIsRunning = false;

GUI.meteorDirectory = null;

GUI.startMeteor = function () {
    if(!GUI.meteorDirectory) {
        return;//TODO add error handling.
    }


    GUI.meteorIsRunning = true;

    meteorProcess  = spawn('/usr/local/bin/meteor', [], {
        cwd: GUI.meteorDirectory,
        env: process.env
    });

    meteorProcess.stdout.on('data', function (data) {
        outputHandler('' + data);

    });

    meteorProcess.stderr.on('data', function (data) {
        outputHandler('' + data);
    });


    meteorProcess.on('close', function (code, signal) {
        if(GUI.meteorIsRunning) {
            GUI.meteorIsRunning = false;
        }
        outputHandler('Meteor Stopped');
        runningWatcher();
    });

    runningWatcher();
};

GUI.getReady = function (callback) {
    readyHandler = callback || function () {};
};

GUI.stopMeteor = function () {
    GUI.meteorIsRunning = false;
    meteorProcess.kill('SIGHUP');
    runningWatcher();
};

GUI.getOutput = function (callback) {
    outputHandler = callback || function () {};
};

GUI.watchMeteor = function (callback) {
    runningWatcher = callback || function (){};
};

GUI.checkMeteorDirectory = function (callback) {
    callback = callback || function () {};
    fs.open(GUI.meteorDirectory + '/.meteor/release','r',function(err,fd){
        if (err && err.code=== 'ENOENT') {
            meteorDirectoryValid = false;
            callback(false);
            return;
        }

        meteorDirectoryValid = true;

        if(readyToStart()) {
            readyHandler(true);
        } else {
            readyHandler(false);
        }

        callback(true);
    });
};

GUI.checkNginxStatus = function (callback) {
    callback = callback || function () {};

    ps.lookup({
        command: 'nginx:',
        psargs: ['aux']
    }, function(err, resultList ) {
        if (err) {
            nginxRunning = false;
            callback(false);
            return;
        }

        if(resultList.length === 0) {
            nginxRunning = false;
            callback(false);
            return;
        }

        nginxRunning = true;

        if(readyToStart()) {
            readyHandler(true);
        } else {
            readyHandler(false);
        }

        callback(true);
    });
};

GUI.startNginx = function (passwordCallback, callback) {
    callback = callback || function() {};
    passwordCallback = passwordCallback || function () {};

    var sudo = require('sudo');
    var options = {
        cachePassword: false,
        prompt: 'NEED_PASSWORD',
        spawnOptions: {
            env: process.env
        }
    };

    var child = sudo(['/usr/local/bin/nginx'], options);
    child.on('close', function () {
        //outputHandler("Nginx is closed ");
        GUI.checkNginxStatus(callback);
    });

    child.stderr.on('data', function (data) {
        //outputHandler("Error reporting for duty " + data);
        if(data.toString() === "#node-sudo-passwd#") {
            passwordCallback(function (newPassword) {
                child.stdin.write(newPassword + '\n');
            });
        }
    });
};


module.exports = GUI;

