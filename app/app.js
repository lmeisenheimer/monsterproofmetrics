;(function() {
    function config($routeProvider, $locationProvider, $httpProvider, $compileProvider) {

        console.log("In here");
        $locationProvider.html5Mode(false);

        // routes
        $routeProvider
          .when('/', {
            templateUrl: '../views/home.html',
            controller: 'MainController',
            controllerAs: 'main'
          })
          .otherwise({
            redirectTo: '/'
          });

        $httpProvider.interceptors.push('authInterceptor');
    }

    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider'];

    angular.module('monsterproofAnalytics', [
      'ngRoute'
    ]).config(config);

    function authInterceptor($rootScope, $q, LocalStorage, $location) {
        console.log("Run is running");
        return {

          // intercept every request
          request: function(config) {
            config.headers = config.headers || {};
            return config;
          },

          // Catch 404 errors
          responseError: function(response) {
            if (response.status === 404) {
              $location.path('/');
              return $q.reject(response);
            } else {
              return $q.reject(response);
            }
          }
        };
    }
    
    

    authInterceptor.$inject = ['$rootScope', '$q', 'LocalStorage', '$location'];

    angular.module('monsterproofAnalytics').factory('authInterceptor', authInterceptor);
    
    function run($rootScope, $location) {
        console.log("Right here");
    }
    
    run.$inject = ['$rootScope', '$location']; 
    
    angular.module('monsterproofAnalytics').run(run);
    console.log("In theory we are running now");
})();