;(function() {
    var  MainController = function (LocalStorage, QueryService) {
        var self = this;
        console.log("loading the main controller");
    };

    MainController.$inject = ['LocalStorage', 'QueryService'];

    angular
        .module('monsterproofAnalytics')
        .controller('MainController', MainController);
})();